const Factura = require('../models/factura')



// insertar una nueva factura
const createFactura = async (req, res) =>{
    try {
        const {Encabezado,Detalle} = req.body
        
        const factura = await Factura.create({
            Encabezado,
            Detalle
        })
        res.send({status:'OK', data:factura})
    } catch (error) {
        console.log('error al crear la factura', error)
       res.status(500).send({status: 'ERROR', data: error})        
    }
}


// muestra las facturas
const verFacturas = async (req, res) =>{
    const facturas = await Factura.find()
    res.send({status: 'OK', data:facturas})    
    
}


//Borrar una factura 
const borrarFactura = async ( req, res ) =>{
    const factura_borrada = req.query.id
    const factura = await Factura.findById(factura_borrada)

    if(!factura){
        res.send({status: 'Factura no encontrada' })
    }

    await Factura.findByIdAndDelete(factura_borrada)
    res.send({status: '¡Factura Borrada con exito!' })
}


// actualizar una  factura 
const actualizarFactura = async ( req, res) =>{
    try {
        const factura_id = req.body.codigo_factura
        const datos = req.body
        //const factura_actualizada = await Factura.findByIdAndUpdate(factura_id, datos, {new:true})
        await Factura.findByIdAndUpdate(factura_id, datos)
        res.send({status: 'OK', message: '¡Factura Actualizada!' })
    } catch (error) {   
        res.send({ status: 'Error', message: '¡Factura no Existe!'})
    }
}

//todo: exporto mis funciones 
module.exports = {
    createFactura,
    verFacturas,
    borrarFactura,
    actualizarFactura
}