const { text } = require('body-parser')
const mongoose = require('mongoose')
const {Schema} = mongoose

const facturaSchema = new Schema({
    Encabezado:{
        IdDoc:{numFact:{type: Number, require: true}},
        Emisor:{RTNEmisor: {type: Number, require: true}},
        Cliente:{
            RTNCliente:{type: Number, require: true},
            RznSocCliente:{type: String, require:true},
            DirCliente: {type: String, require: true},
            TelefonoCliente: {type: Number, require:true},
            CiudadCliente: {type:String, require:true}
        }
    },
    Detalle:[{
        NmbItem:{type:String, require:true},
        DscItem:{type:String, require:true},
        QtyItem: {type:Number, require:true},
        UnmdItem:{type:String, require:true},
        PrcItem:{type:Number, require:true}
    }
]
})

const modelo =  mongoose.model('Factura',facturaSchema)
module.exports = modelo