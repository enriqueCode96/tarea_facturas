const express = require('express');
const controladorFactura = require('../controller/controladorFactura')
const router = express.Router()

//todo creo mis rutas de los controladores
router.post('/creando-factura', controladorFactura.createFactura)
router.get('/get-facturas', controladorFactura.verFacturas)
router.delete('/borrar-factura', controladorFactura.borrarFactura)
router.put('/actualizar-factura', controladorFactura.actualizarFactura)

// exporto los router
module.exports = router