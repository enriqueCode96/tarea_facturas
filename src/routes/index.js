const facturaRoutes = require('./factura-routes')

module.exports = (app) =>{
    app.use('/api/facturas', facturaRoutes)
}