const express = require('express')
const bodyParser = require('body-parser')
const dotenv = require('dotenv')
const mongoose = require('mongoose')


dotenv.config()

const rutas = require('./routes')
const app = express()

app.use(bodyParser.urlencoded({ extended:false}))
app.use(bodyParser.json())

rutas(app)

const PORT = process.env.PORT || 4000

//todo: creo la conxecion con la base de MongoDB
mongoose.connect(process.env.MONGO,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true

}).then(()=>{
    console.log(`Conectado con MongoDB`)

    app.listen(PORT,() =>{
        console.log(`corriendo en el puerto:${PORT}`)
    })
}).catch((error)=>{
    console.log('error al conectarse con Mongo', error)
})

